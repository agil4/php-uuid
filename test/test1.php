<?php
use Agil4\Uuid\DbMap;
use Agil4\Uuid\NSDBGenerator;
use Agil4\Uuid\NSGenerator;
use Agil4\Uuid\RandomGenerator;

require_once "../vendor/autoload.php";

$nsGen = new NSGenerator('9d500263-e1a3-552b-863f-aaca4e2bd9b7');
$uu = $nsGen->generate(1);
echo $uu;

echo "\r\n";

$pdo = new PDO("mysql:dbname=dev_sccweb_mobile;host=192.168.1.12;port=3336", "admdev", "info@123");
$nsDbGen = new NSDBGenerator($pdo, new DbMap("dispositivo", "uuid", array("id" => 1)));
$uuid = $nsDbGen->generate(1);
echo $uuid;

echo "\r\n";

$gen = new RandomGenerator();
echo $gen->generate();

echo "\r\n";