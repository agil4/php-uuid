<?php
namespace Agil4\Uuid;

use Agil4\Uuid\Library\Uuid as InterfaceUuid;

class Uuid implements InterfaceUuid
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * @param string $uuid
     */
    public function __construct($uuid)
    {
        $this->uuid = $uuid;
        if (!is_string($uuid)) {
            throw new \InvalidArgumentException("Uuid não é uma string");
        } elseif ($this->isValid() === false) {
            throw new \InvalidArgumentException("Uuid não é válido");
        }
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->uuid;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return \Rhumsaa\Uuid\Uuid::isValid($this->uuid);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }
}