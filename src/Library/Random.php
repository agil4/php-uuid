<?php
namespace Agil4\Uuid\Library;

interface Random
{
    /**
     * @return Uuid;
     */
    public function generate();
}