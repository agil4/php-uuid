<?php
namespace Agil4\Uuid\Library;

interface Generator
{
    /**
     * @param $name
     * @return Uuid
     */
    public function generate($name);
} 