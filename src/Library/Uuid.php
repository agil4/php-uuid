<?php
namespace Agil4\Uuid\Library;

use InvalidArgumentException;

interface Uuid
{
    /**
     * @param string $uuid
     * @throws InvalidArgumentException
     */
    public function __construct($uuid);

    /**
     * @return string
     */
    public function toString();

    /**
     * @return bool
     */
    public function isValid();
}