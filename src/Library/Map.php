<?php
namespace Agil4\Uuid\Library;

interface Map
{
    /**
     * @param string $tableName
     * @param string $columnName
     * @param array $filter
     */
    public function __construct($tableName, $columnName, Array $filter);

    /**
     * @return string
     */
    public function getTableName();

    /**
     * @return string
     */
    public function getColumnName();

    /**
     * @return array
     */
    public function getFilter();
} 