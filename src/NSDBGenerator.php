<?php
namespace Agil4\Uuid;

use Agil4\Uuid\Library\Generator;
use Agil4\Uuid\Library\Map;
use PDO;
use Rhumsaa\Uuid\Uuid as RUuid;

class NSDBGenerator implements Generator
{
    /**
     * @var Map
     */
    private $map;

    private $conn;

    /**
     * @param PDO $connection
     * @param Map $dbMap
     */
    public function __construct(PDO $connection, Map $dbMap)
    {
        $this->map = $dbMap;
        $this->conn = $connection;
    }

    /**
     * @param $name
     * @return Uuid
     */
    public function generate($name)
    {
        $sql = "SELECT {$this->map->getColumnName()}
            FROM {$this->map->getTableName()} WHERE 1 = 1";

        foreach ($this->map->getFilter() as $filter => $value) {
            $sql .= " AND {$filter} = :{$filter}";
        }

        $sth = $this->conn->prepare($sql);
        $sth->execute($this->map->getFilter());

        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            $ns = new Uuid($result['uuid']);
        } else {
            throw new \InvalidArgumentException("Uuid não localizado no banco de dados");
        }

        $uuid = RUuid::uuid5($ns->toString(), $name)->toString();
        return new Uuid($uuid);
    }
}