<?php
namespace Agil4\Uuid;

use Agil4\Uuid\Library\Random;
use Rhumsaa\Uuid\Uuid as RhumsaaUuid;

class RandomGenerator implements Random
{
    /**
     * @return Uuid
     */
    public function generate()
    {
        $uuid4 = RhumsaaUuid::uuid4()->toString();
        return new Uuid($uuid4);
    }
}