<?php
namespace Agil4\Uuid;

use Agil4\Uuid\Library\Generator;
use Rhumsaa\Uuid\Uuid as RhumsaaUuid;

class NSGenerator implements Generator
{
    /**
     * @var Uuid
     */
    private $nsUuid;

    /**
     * @param $namespace
     */
    public function __construct($namespace)
    {
        $this->nsUuid = new Uuid($namespace);
    }

    /**
     * @param $name
     * @return \Agil4\Uuid\Uuid
     */
    public function generate($name)
    {
        $uuid = RhumsaaUuid::uuid5($this->nsUuid->toString(), $name)->toString();
        return new Uuid($uuid);
    }
}