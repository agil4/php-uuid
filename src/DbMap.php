<?php
namespace Agil4\Uuid;

use Agil4\Uuid\Library\Map;

class DbMap implements Map
{
    /**
     * @var string
     */
    private $table;

    /**
     * @var string
     */
    private $column;

    /**
     * @var array
     */
    private $filter = array();

    /**
     * @param string $tableName
     * @param string $columnName
     * @param array $filter
     */
    public function __construct($tableName, $columnName, Array $filter)
    {
        $this->table = $tableName;
        $this->column = $columnName;
        $this->filter = $filter;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getColumnName()
    {
        return $this->column;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        return $this->filter;
    }
}